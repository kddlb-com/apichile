const express = require('express');
const bodyParser = require('body-parser');
const correosChile = require('./services/correoschile');

let app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

let port = process.env.PORT || 8080;
let router = express.Router();

router.get('/', function (req, res) {
    res.json({message: 'hooray! welcome to our api!'});
});

app.use('/api', router);
app.use('/api/correosChile', correosChile);

app.listen(port);
console.log('Running on port ' + port);
