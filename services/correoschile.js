const express = require('express');
const request = require('request');
const cheerio = require('cheerio');
const moment = require('moment');
const router = express.Router();

moment.fn.toJSON = function () {
    return this.format();
};


router.get("/:trackingCode", (req, res) => {
    const trackingCode = req.params.trackingCode;

    request.post({
        url: 'http://seguimientoweb.correos.cl/ConEnvCorreos.aspx',
        form: {
            obj_key: "Cor398-cc",
            obj_env: trackingCode
        }
    }, (err, rsp, bdy) => {
        const $ = cheerio.load(bdy);

        let result;

        if ($("#pnlError").length > 0) {
            result = {
                error: $(".envio_no_existe").text()
            }
        } else {
            const trackingTable = $("table.tracking");
            let resultStore = [];
            $("tbody>tr", trackingTable).each((i, e) => {
                let row = $(e).find("td");
                resultStore.push({
                    date: moment($(row[1]).text().trim(), "DD/MM/YYYY H:mm"),
                    location: $(row[2]).text().trim(),
                    description: $(row[0]).text().trim(),

                });
            });

            let deliveryInfo;

            if ($(".datosgenerales").length > 0) {
                let shipmentNumber =
                    $("#Panel_Entrega > table > tbody > tr:nth-child(1) > td:nth-child(2)").text().trim();
                let deliveryDate =
                    moment($("#Panel_Entrega > table > tbody > tr:nth-child(2) > td:nth-child(2)").text().trim(),
                        "DD/MM/YYYY H:mm");
                let deliveredTo =
                    $("#Panel_Entrega > table > tbody > tr:nth-child(1) > td:nth-child(4)").text().trim();
                let deliveredRut =
                    $("#Panel_Entrega > table > tbody > tr:nth-child(2) > td:nth-child(4)").text().trim();

                deliveryInfo = {
                    shipmentNumber, deliveryDate, deliveredTo: {
                        name: deliveredTo,
                        rut: deliveredRut
                    }
                }
            }

            resultStore.shift();
            result = {deliveryInfo: deliveryInfo, events: resultStore};
        }

        res.json(result)
    });


});

module.exports = router;